<?php
    include_once './Models/Products.php';

    class ProductController extends Products {

        public  function __construct()
        {
            parent::__construct();

            $method = 'product';
            if(isset($_GET['method'])) {
                $method = $_GET['method'];
            }

            switch ($method) {
                case 'product':
                    $this->homePage();
                    break;

                default:
                    echo 'Page 404 not found';
                    break;
            }
        }

        public function homePage() {
            $results = parent::homePage();
            $commings = parent::commingProduct();

            include_once './pages/products/view_product.php';
        }

    }
