<?php
    session_start();
    ob_start();
?>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<!-- Head -->
	<?php
		include_once 'includes/head.php';
	?>
<!-- End head -->
<body>

	<!-- Start Header Area -->
	<?php
		include_once 'layout/header.php';
	?>
	<!-- End Header Area -->

	<!-- start banner Area -->
	<?php
		include_once 'layout/banner.php';
	?>
	<!-- End banner Area -->

	<!-- start features Area -->
	<?php
		include_once 'layout/features.php';
	?>
	<!-- end features Area -->

	<!-- Start category Area -->
	<?php
		include_once 'layout/category.php';
	?>
	<!-- End category Area -->

	<!-- start product Area -->
                <?php
                    $page = 'product';
                    if(isset($_GET['page'])) {
                        $page = $_GET['page'];
                    }
                    switch ($page) {
                        case 'product':
                            include_once './Controller/ProductController.php';
                            $product = new ProductController;
                            break;


                        default:
                            echo 'Page 404 not found';
                            break;
                    }
                ?>
	<!-- end product Area -->

	<!-- Start exclusive deal Area -->
	<?php
		include_once 'layout/deal.php';
	?>
	<!-- End exclusive deal Area -->

	<!-- Start brand Area -->
	<?php
		include_once 'layout/brand.php';
	?>
	<!-- End brand Area -->

	<!-- Start related-product Area -->
	<?php
		include_once 'layout/related-product.php';
	?>
	<!-- End related-product Area -->

	<!-- start footer Area -->
	<?php
		include_once 'layout/footer.php';
	?>
	<!-- End footer Area -->

	<!-- Script -->
	<?php
		include_once 'includes/script.php';
	?>
	<!-- End script -->
</body>

</html>